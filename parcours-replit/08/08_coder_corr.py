liste_notes = []  # (a) on crée une liste vide

# (b) boucle while pour entrer les notes
note = 9 # on donne une valeur qui permette d'entrer dans la boucle
while note >= 0 : # une note < 0 met fin à la boucle
    note = int(input("Entrer une note (une note négative pour quitter) : "))
    if note >= 0 :
        liste_notes.append(note)  # on ajoute la note à la liste des notes

print("Liste des notes :", liste_notes)  # (c) affichage de la liste des notes

somme = 0  # (d) une variable somme qui vaut 0
# (e) boucle while pour effectuer la somme des notes
index = 0  # on donne une valeur qui permette d'entrer dans la boucle
while index != len(liste_notes) :
    somme = somme + liste_notes[index]
    index = index + 1

print(somme) # (f) on affiche la somme des notes
moyenne = somme / len(liste_notes) # (g) on calcule la moyenne...

print("Moyenne : ", moyenne)   # ...et on l'affiche