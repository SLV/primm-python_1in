print(int("32"))    # conversion de la chaîne de caractères "32" en nombre entier

print(str(71))      # conversion de l'entier 71 en chaîne de caractères

print(float("4.5")) # conversion de la chaîne de caractères "4.5" en nombre à virgule

print(int(4.5))     # conversion du nombre à virgule 4.5 en nombre entier

print(float(5))     # conversion du nombre entier 5 en nombre à virgule