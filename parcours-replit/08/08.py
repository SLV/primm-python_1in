liste_cantons = ["Genève", "Neuchâtel", "Jura"]

print("Voici la liste des cantons : ", liste_cantons)
print("Il y a", len(liste_cantons), "cantons dans la liste.")

reponse = None
while reponse != "fin" :
    reponse = input("Entrer le nom d'un canton ou 'fin' pour quitter : ")
    if reponse in liste_cantons :
        print("Le canton de " + reponse + " est déjà dans la liste !")
    else :
        if reponse != "fin" :
            liste_cantons.append(reponse)

print("Nouvelle liste des cantons :", liste_cantons)

reponse = input("Entrer le nom d'un canton à supprimer de la liste : ")
liste_cantons.remove(reponse)
print("Nouvelle liste des cantons :", liste_cantons)