liste_2d = [[11,12,13],
            [21,22,23],
            [31,32,33]]

# attention, les index démarrent à 0 ! on a trois lignes : 0, 1 et 2
# et trois colonnes : 0, 1 et 2
# on doit écrire le numéro de ligne puis celui de colonne.
print(liste_2d[1][2]) # affiche la valeur 23 (ligne 1, colonne 2)
print(liste_2d[0][1]) # affiche la valeur 12 (ligne 1, colonne 2)
liste_2d[2][2] = "trente-trois" # on modifie l'élément ligne 2 colonne 2
print(liste_2d)  # pour vérifier que la modification a bien eu lieu