nom_programme = "Le nombre secret…"
ligne = "*" * 60

print(ligne)   # ces trois lignes affichent le titre
print(" " * 20 + nom_programme)
print(ligne)

nb_secret = 5     # le nombre secret à deviner

print("Je pense à un nombre compris entre 0 et 10")
essai = int(input("Peux-tu le deviner ? "))

if essai < nb_secret :
    print("Trop petit !")
    
if essai > nb_secret :
    print("Trop grand !")
    
if essai == nb_secret :
    print("Bravo !")
    print("Vous avez trouvé le nombre secret !")

print(ligne)   # fin du programme