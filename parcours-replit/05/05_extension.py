import random

a = "Nom du programme"
b = "*" * 60

print(b)   # ces trois lignes affichent le titre
print(" " * 20 + a)
print(b)

for i in range(20) :
    nb_secret = random.randint(0,10)     # le nombre secret à deviner
    print(nb_secret)

print("Je pense à un nombre compris entre 0 et 10")
c = int(input("Peux-tu le deviner ? "))

if c < nb_secret :
    print("Trop petit !")
    
if c > nb_secret :
    print("Trop grand !")
    
if c == nb_secret :
    print("Bravo !")
    print("Vous avez trouvé le nombre secret !")

print(b)   # fin du programme