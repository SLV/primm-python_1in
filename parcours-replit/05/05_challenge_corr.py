annee = int(input("Entrez une année : "))

if annee % 4 == 0 :
    bissextile = True
else :
    bissextile = False
    
if annee % 100 == 0 :
    bissextile = False
    
if annee % 400 == 0 :
    bissextile = True

if bissextile == True :
    print("L'année est bissextile")
else:
    print("L'année n'est pas une année bissextile!")