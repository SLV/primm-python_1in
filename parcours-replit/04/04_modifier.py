nb1 = "21" # on démarre avec une chaîne de caractères affectée à la variable nb1
print("Contenu de la variable :", nb1, "- type de la variable :", type(nb1), "- résultat de 2 × variable =", 2 * nb1)
nb1 = int(nb1)
print("Contenu de la variable :", nb1, "- type de la variable :", type(nb1), "- résultat de 2 × variable =", 2 * nb1)

nb2 = 7

print(nb2, type(nb2))
print(nb1, nb2)
print(nb1 + nb2)
print(nb2 * 5)
print("21 fois 7 =", nb1 * nb2)
print(nb1 / nb2)
print("Type de nb1 / nb2 :", type(nb1 / nb2))
calcul = (nb1 - 2) * nb2  # les parenthèses forcent la soustraction avant la multiplication
print(calcul)
nb3 = int(input("Entrer un nombre entier : "))
nb1 = nb1 + nb3
calcul = (nb1 - 2) * nb2  # il faut refaire le calcul et l'affecter à la variable calcul !
print(calcul)