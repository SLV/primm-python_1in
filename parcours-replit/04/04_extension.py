import math # on importe le module 'math'

print(math.sqrt(20)) # racine carrée de 20

print(math.gcd(20, 25)) # plus grand diviseur commun de 20 et 25

print(math.pi) # la constante pi

print(math.cos(1.5)) # cosinus d'un angle donné en radian

print(27//4)
