# (a) On entre les dimensions du trapèze
p_base = int(input("Quelle est la longueur de la petite base ? "))
g_base = int(input("Quelle est la longueur de la grande base ? "))
hauteur = int(input("Quelle est la longueur de la hauteur ? "))

# (b) Calcul de l'aire du trapèze
aire = ((p_base + g_base) * hauteur) / 2

# (c) On affiche l'aire
print("L'aire du trapèze vaut :", aire)
