age = int(input("Quel âge avez-vous ? "))  # (a) on demande l'âge

# (b) structure conditionnelle
if age < 3 :       # plus petit que 3, non accepté                        
    print("Tu ne peux pas entrer dans le cinéma, tu n'est pas assez grand.")
elif age >= 3 and age <= 12 : # entre 3 et 12 compris, tarif enfant
    print("Tarif enfant : CHF 15,90")
elif age >= 13 and age <= 17 : # entre 13 et 17 compris, tarif réduit
    print("Tarif réduit : CHF 18,90")
else :  # plein tarif pour les autres
    print("Tarif adulte : CHF 21,90")