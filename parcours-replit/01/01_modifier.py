import time # on importe le module 'time'

print("Bienvenue en 1ère année d'informatique DF !")
time.sleep(2) # 2 secondes de pause

print("L'interpréteur Python lit chaque ligne de code et l'exécute (l'interprète).")
time.sleep(4) # 4 secondes de pause

print("Tout ce qui suit le caractère # n'est pas pris en compte par l'interpréteur Python.") # ainsi, ceci n'est pas pris en compte, c'est un commentaire
time.sleep(3) # 3 secondes de pause

print("Avec l'instruction \"print()\", il est possible d'écrire quelque chose à l'écran.")
time.sleep(5) # 5 secondes de pause

print("La contre-oblique ou anti-slash \\ permet d'utiliser des caractères spéciaux dans les chaînes de caractères.")
time.sleep(1) # 1 secondes de pause

print("Les chaînes de caractères sont entourées par des guillemets doubles (\") ou simples (').")
time.sleep(2)

print("On peut effectuer un retour à la ligne\n avec le caractère spécial \\n.")
time.sleep(2)

# print()  <-- ce print est supprimé
print("\nÀ bientôt !")  # et on ajoute le saut de ligne dans ce print
