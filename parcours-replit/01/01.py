import time # on importe le module 'time'

print("Bienvenue dans la programmation Python !")
time.sleep(2)

print("L'interpréteur Python lit chaque ligne de code et l'exécute (l'interprète).")
time.sleep(2)

print("Tout ce qui suit le caractère # n'est pas pris en compte par l'interpréteur Python.") # ainsi, ceci n'est pas pris en compte, c'est un commentaire
time.sleep(2)

print("Avec l'instruction print(), il est possible d'écrire quelque chose à l'écran.")
time.sleep(2)

print("La contre-oblique ou anti-slash \\ permet d'utiliser des caractères spéciaux dans les chaînes de caractères.")
time.sleep(2)

print("Les chaînes de caractères sont entourées par des guillemets doubles (\") ou simples (').")
time.sleep(2)

print("On peut effectuer un retour à la ligne\n avec le caractère spécial \\n.")
time.sleep(2)

print()
print("À bientôt !")
