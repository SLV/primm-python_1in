# (a) les quatre listes vides
liste_objets = []
liste_description = []
liste_prix = []
liste_achat = []

# (b) une boucle for produisant 5 itérations
for i in range(5) :
    objet = input("Donner le nom d'un objet : ") # on demande le nom de l'objet
    liste_objets.append(objet)   # et on l'ajoute à la liste

# (c) une boucle for qui parcourt la liste des objets
for objet in liste_objets :
    print("Objet :", objet) # affichage de l'objet en cours
    description = input("Ajouter une description : ") # on demande la description
    liste_description.append(description) # ajout de la description à la liste des descriptions
    prix = int(input("Ajouter un prix : ")) # on demande le prix
    liste_prix.append(prix) # ajout du prix à la liste des prix

# (d) une boucle for produisant un nombre d'itérations égal à la longueur de la liste_objets
for i in range(len(liste_objets)) :
    print(liste_objets[i],":", liste_description[i]," -  Prix :", liste_prix[i],"CHF")
    quantite = int(input("Combien voulez-vous en acheter ? ")) # on demande la quantité souhaité
    liste_achat.append(quantite) # on ajoute cette quantité à la liste des quantités

# (e) une boucle for qui calcule le montant à payer
somme = 0
for i in range(len(liste_objets)) : # autant d'itération que d'objet
    somme = somme + liste_prix[i] * liste_achat[i] # calcul du montant total

print("Montant dû :", somme, "CHF") # (f) affichage du montant dû