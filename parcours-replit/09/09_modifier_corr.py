liste_fruits = ["kiwi", "cerise", "ananas"]

print("Liste des fruits :")
for fruit in liste_fruits :
    print("Fruit n°", liste_fruits.index(fruit),":",fruit)

print("On va ajouter 4 nouveaux fruits dans la liste.")
for compteur in range(4) :
    print("On va ajouter le fruit n°", compteur + len(liste_fruits), "dans la liste.", end=" ")
    fruit = input("Donner le nom de ce fruit : ")
    if fruit in liste_fruits :
        print("Ce fruit est déjà dans la liste !")
    else :
        liste_fruits.append(fruit)

print("Nouvelle liste de fruits :", liste_fruits)

index = int(input("Donner le numéro d'index du fruit à épeler : "))
compteur = 1
for lettre in liste_fruits[index] :
    print("Lettre n°", compteur, ":", lettre)
    compteur = compteur + 1

print("Type de la variable 'liste_fruits' :",type(liste_fruits))