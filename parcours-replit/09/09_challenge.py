print("Les livrets de 0 à 10 :")
for nb1 in range(11) :
    print("-" * 6 + " Livret de : " + str(nb1))
    for nb2 in range(11) :
        print(str(nb1) + " fois " + str(nb2) + " = " + str(nb1 * nb2))