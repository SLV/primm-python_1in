# Affectation des variables
salutation = "Bonjour"
texte = "mon nom est"
prenom = "John"
nom = "Lennon"

# Affichages
print(salutation)
print(prenom, nom)
print("Salut, je m'appelle", prenom, nom, "!")
print(prenom + " " + nom)  # concaténation de str
print((nom + " ") * 5) # concaténation de str

print(type(salutation))

print(prenom[0])
print(nom[0])

presentation = salutation + ",\n" + texte + " " + prenom + " " + nom # concaténation de str
print(presentation)