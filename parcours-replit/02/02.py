# Affectation des variables
a = "Bonjour"
b = "mon nom est"
prenom = "Jean-Jacques"
nom = "Rousseau"

# Affichages
print(a)
print(prenom, nom)
print("Salut", prenom, nom, "!")
print(prenom + nom)
print(nom * 5)

print(type(a))

print(a[0])
print(a[3] * 5)

presentation = a + ", " + b + " " + prenom + " " + nom
print(presentation)