# (a) Affectation des variables
prenom = "John"
humeur = "très bonne"

# Affichages
# (b)
print("Prénom :", prenom)
print("Humeur :", humeur)
# (c)
print("Salut, je m'appelle " + prenom + " !")
# (d)
print("Aujourd'hui, je suis de", humeur, "humeur.")
# (e)
print("Salut, je m'appelle " + prenom + " !\n" + "Aujourd'hui, je suis de " + humeur + " humeur.")
