# une fonction classique
def f(x):
    return x**2

# une fonction lambda ou fonction anonyme
g = lambda x : x**2

print(f(5))
print(g(5))