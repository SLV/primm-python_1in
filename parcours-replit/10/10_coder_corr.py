# Fonctions :
def plus_grand_liste_2(l) :
    if l[0] > l[1] :
        return(l[0])
    else :
        return(l[1])
    
def plus_grand_des_3(a, b, c) :
    if a >= b and a >= c :
        return(a)
    elif b >= a and b >= c :
        return(b)
    else :
        return(c)

def plus_grand_liste_3(l) :
    if l[0] >= l[1] and l[0] >= l[2] :
        return(l)
    elif l[1] >= l[0] and l[1] >= l[2] :
        temp = l[0]
        l[0] = l[1]
        l[1] = temp
        return(l)
    else :
        temp = l[0]
        l[0] = l[2]
        l[2] = temp
        return(l)

def triangle_constructible(l) :
    if l[1] + l[2] > l[0] :
        return(True)
    else :
        return(False)

def triangle_rectangle(l) :
    if l[0]**2 == l[1]**2 + l[2]**2 :
        return(True)
    else :
        return(False)

# Les lignes suivantes permettent de tester les fonctions :
print(plus_grand_liste_2([23, 37]))
print(plus_grand_des_3(39, 109, 12))
print(plus_grand_liste_3([3, 5, 4]))
print(triangle_constructible([5, 3, 4]))
print(triangle_rectangle([5, 3, 4]))

# Programme principal :
liste_des_cotes = []
for i in range(3) :
    cote = int(input("Entrez la longueur d'un côté du triangle : "))
    liste_des_cotes.append(cote)
    
plus_grand_liste_3(liste_des_cotes)
print("La liste des côtés du triangle est :", liste_des_cotes)

if triangle_constructible(liste_des_cotes) :
    print("Votre triangle est constructible")
    if triangle_rectangle(liste_des_cotes) :
        print("De plus, il est rectangle")
    else :
        print("Mais il n'est pas rectangle")
else :
    print("Votre triangle n'est pas constructible !")