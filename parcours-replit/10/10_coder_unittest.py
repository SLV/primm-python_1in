import unittest

# Fonctions :
def plus_grand_liste_2(l) :
    if l[0] > l[1] :
        return(l[0])
    else :
        return(l[1])
    
def plus_grand_des_3(a, b, c) :
    if a >= b and a >= c :
        return(a)
    elif b >= a and b >= c :
        return(b)
    else :
        return(c)

def plus_grand_liste_3(l) :
    if l[0] >= l[1] and l[0] >= l[2] :
        return(l)
    elif l[1] >= l[0] and l[1] >= l[2] :
        temp = l[0]
        l[0] = l[1]
        l[1] = temp
        return(l)
    else :
        temp = l[0]
        l[0] = l[2]
        l[2] = temp
        return(l)

def triangle_constructible(l) :
    if l[1] + l[2] > l[0] :
        return(True)
    else :
        return(False)

def triangle_rectangle(l) :
    if l[0]**2 == l[1]**2 + l[2]**2 :
        return(True)
    else :
        return(False)

# Les lignes suivantes permettent de tester les fonctions :
print(plus_grand_liste_2([23, 37]))
print(plus_grand_des_3(39, 109, 12))
print(plus_grand_liste_3([3, 5, 4]))
print(triangle_constructible([5, 3, 4]))
print(triangle_rectangle([5, 3, 4]))

# Programme principal :
liste_des_cotes = []
for i in range(3) :
    cote = int(input("Entrez la longueur d'un côté du triangle : "))
    liste_des_cotes.append(cote)
    
plus_grand_liste_3(liste_des_cotes)
print("La liste des côtés du triangle est :", liste_des_cotes)

if triangle_constructible(liste_des_cotes) :
    print("Votre triangle est constructible")
    if triangle_rectangle(liste_des_cotes) :
        print("De plus, il est rectangle")
    else :
        print("Mais il n'est pas rectangle")
else :
    print("Votre triangle n'est pas constructible !")


# Tests unitaires :
class Test(unittest.TestCase):
    # unittest.main() ou test -v fichier.py
    def test_plus_grand_liste_2(self):
        self.assertEqual(plus_grand_liste_2([93, 57]), 93)
        self.assertEqual(plus_grand_liste_2([57, 57]), 57)
        self.assertEqual(plus_grand_liste_2([1, 5]), 5)
        
    def test_plus_grand_des_trois(self):
        self.assertEqual(plus_grand_des_3(42, 98, 71), 98)
        self.assertEqual(plus_grand_des_3(2, 2, 2), 2)
        self.assertEqual(plus_grand_des_3(100, 7, 12), 100)
        self.assertEqual(plus_grand_des_3(33, -12, 34), 34)
        
    def test_plus_grand_liste_3(self):
        self.assertEqual(plus_grand_liste_3([93, 57, 124]), [124, 57, 93])
        self.assertEqual(plus_grand_liste_3([12, 289, 34]), [289, 12, 34])
        self.assertEqual(plus_grand_liste_3([9, 5, 1]), [9, 5, 1])
    
    def test_triangle_constructible(self):
        self.assertEqual(triangle_constructible([5, 3, 4]), True)
        self.assertEqual(triangle_constructible([5, 3, 4]), True)
        self.assertEqual(triangle_constructible([10, 3, 4]), False)
        self.assertEqual(triangle_constructible([7, 3, 4]), False)
        
    def test_triangle_rectangle(self):
        self.assertEqual(triangle_rectangle([5, 3, 4]), True)
        self.assertEqual(triangle_rectangle([5, 4, 3]), True)
        self.assertEqual(triangle_rectangle([3, 4, 5]), False)
        self.assertEqual(triangle_rectangle([5, 2, 4]), False)