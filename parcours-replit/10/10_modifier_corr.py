def une_ligne() :
    return("·" * 60)

def phrase_centree(phrase) :
    nb_espaces = int((60 - len(phrase)) / 2)
    return(" " * nb_espaces + phrase + " " * nb_espaces)

def phrase_entre_lignes(phrase) :
    return(une_ligne() + "\n" + phrase_centree(phrase) + "\n" + une_ligne())

def retire_voyelles(mot) :
    voyelles = ["a", "e", "i", "o", "u", "y", "A", "E", "I", "O", "U", "Y"]
    mot_sans_voyelle = mot[0]
    for lettre in mot[1:] :
        if lettre not in voyelles :
            mot_sans_voyelle = mot_sans_voyelle + lettre
    return(mot_sans_voyelle)

def retire_espaces(mot) :
    mot_sans_espace = ""
    for lettre in mot :
        if lettre != " " :
            mot_sans_espace = mot_sans_espace + lettre
    return(mot_sans_espace)

def cree_login(mot1, mot2) :
    mot1 = retire_espaces(mot1)
    mot2 = retire_espaces(mot2)
    login = mot1 + "." + retire_voyelles(mot2)
    return(login)

print(phrase_entre_lignes("Préparation de ton login"))

prenom = input("Entrez votre prénom : ")
nom = input("Entrez votre nom : ")

nom_utilisateur = cree_login(prenom, nom)

print(phrase_entre_lignes("Ton login : " + nom_utilisateur))

