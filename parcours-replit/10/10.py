def une_ligne() :
    return("-" * 60)

def phrase_centree(phrase) :
    nb_espaces = int((60 - len(phrase)) / 2)
    return(" " * nb_espaces + phrase + " " * nb_espaces)

def ma_fonction(mot) :
    voyelles = ["a", "e", "i", "o", "u", "y"]
    mot_sans_voyelle = ""
    for lettre in mot :
        if lettre not in voyelles :
            mot_sans_voyelle = mot_sans_voyelle + lettre
    return(mot_sans_voyelle)

def cree_login(mot1, mot2) :
    login = mot1 + "." + ma_fonction(mot2)
    return(login)

print(une_ligne())
print(phrase_centree("Préparation de ton login"))
print(une_ligne())

prenom = input("Entrez votre prénom : ")
nom = input("Entrez votre nom : ")

nom_utilisateur = cree_login(prenom, nom)

print(une_ligne())
print(phrase_centree("Ton login : " + nom_utilisateur))
print(une_ligne())
