reponse = None      # initialisation de la variable de boucle
                    # on va entrer dans la boucle car None != Genève

while reponse != "Genève" :  # (a) cette boucle tourne tourne tant que reponse != Genève
    reponse = input("Dans quel canton se trouve la ville de Genève ? ") # (b) on pose la question
    if reponse != "Genève" :      # (c) mauvaise réponse ?
        print("Essaie encore !")  # on reste piégé dans la boucle

print("Bravo !")   # (d) on indique que la réponse est correcte

# (e) on fait de même pour une autre question
while reponse != "Berne" and reponse != "Bern" :   # deux réponses possible ici
    reponse = input("Quelle est la capitale de la Suisse ? ")
    if reponse != "Berne" and reponse != "Bern" :
        print("Essaie encore !")

print("Bravo !")   