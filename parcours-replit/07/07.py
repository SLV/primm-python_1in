import time  # on importe le module time

nom_utilisateur = input("Entrez votre nom d'utilisateur : ")
mot_de_passe = "z0rr0"

essai = None
while essai != mot_de_passe :
    essai = input("Entrez votre mot de passe : ")

compteur = 3
while compteur > 0 :
    print(compteur, end=" ")  # end=" " évite le retour à la ligne après print
    time.sleep(1)    # on attend 1 seconde 
    compteur = compteur - 1
    
print("login accepté,", nom_utilisateur, "!")
