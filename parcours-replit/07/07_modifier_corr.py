import time  # on importe le module time

nom_utilisateur = input("Entrez votre nom d'utilisateur : ")
mot_de_passe = input("Choisissez un mot de passe : ")
print(type(nom_utilisateur), type(mot_de_passe))
essai = None
nb_essai = 0
while essai != mot_de_passe :
    nb_essai = nb_essai + 1
    print("Bonjour", nom_utilisateur)
    essai = input("Entrez votre mot de passe : ")

compteur = 5
while compteur > -1 :
    print(compteur, end=" ")  # end=" " évite le retour à la ligne après print
    time.sleep(.3)    # on attend 1 seconde 
    compteur = compteur - 1
    
print("login accepté en" + " " + str(nb_essai) + " " + "tentative(s)" + " " + nom_utilisateur + " " + "!")
