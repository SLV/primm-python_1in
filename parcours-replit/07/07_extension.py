import hashlib # cette bibliothèque donne accès aux fonctions de hachage

motPasse = "abc123"
print("Haché du mot de passe :")
print(hashlib.sha1(motPasse.encode()).hexdigest())

essai = input("Entrer le mot de passe : ")

if hashlib.sha1(essai.encode()).hexdigest() == "6367c48dd193d56ea7b0baad25b19455e529f5ee" :
    print("Login accepté")
else :
    print("Mot de passe faux !")