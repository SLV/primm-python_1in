print("-" * 60) # on affiche une ligne
print("Comment t'appelles-tu ?")
prenom = input("Tape ton prénom puis appuie sur la touche 'Entrée' : ")
print("Bonjour" + " " + prenom) # salutations
# print("Quel genre de musique aimes-tu ?")
style_musique = input("Quel genre de musique aimes-tu ? ")
print(style_musique + " : intéressant !")
activite = input("Quelle activité aimes-tu pratiquer ? Réponse : ")
print("Pratiquer" + " " + activite + " " + "doit être passionnant.")
print("Merci", prenom, "et à bientôt !")
print("-" * 60)
print(prenom + " " + "aime la musique" + " " + style_musique + "\n" + "Il pratique" + " " + activite)