print("\/" * 30) # (a) on affiche une ligne
prenom = input("Comment t'appelles-tu ? ") # (b) entrée du prénom
print("Bonjour" + " " + prenom) # (c) on salue l'utilisateur
chanteur = input("Si tu étais un chanteur anglais, tu serais : ")  # (d) une question
plat = input("Si tu étais un plat cuisiné, tu serais : ") # (e) deux autres questions
odeur = input("Si tu étais une odeur, tu serais : ")
# (f) Affichage d'un résumé en une instruction print avec concaténations
print("Alors " + prenom + " : \n" + "comme chanteur, tu serais " + chanteur + "\n" + "comme plat, " + plat + "\n" + "et comme odeur, " + odeur)
print("\/" * 30) # (g) on affiche une ligne
