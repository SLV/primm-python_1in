# Name
PRIMM-Python_1in

# Description
A course to learn Python from crash in the first year of high school, using the PRIMM method.

# Documentation
Read the doc (./docs/GymInf_SimonVerdan.pdf) to find out all about this pedagogical approach and the series offered in this depot!


# LaTeX
The doc is written with LaTeX. A boolean called Correctif is present in the series_primm.tex file. By choosing \Correctiftrue, the generated pdf document will contain the answers to the questions asked. If you choose \Correctiffalse, only the questions will be present.

# Authors
Simon Verdan

# Licence
Creative Commons CC-BY-SA
